<?php include "header.php" ?>

  <content>
    <div class="container">
      <?php
        $serverip = $_SERVER["SERVER_ADDR"];
        $username = $_SERVER[""];
        $hostname = $_SERVER['HTTP_HOST'];

        print("
            <p>Your server address is " . $serverip . "</p>
            <p>Your webserver is " . $_SERVER['SERVER_SOFTWARE'] . "</p>
            <p>Your webserver name is " . $_SERVER['SERVER_NAME'] . "</p>
            <p>The current PHP version is " . phpversion() . "</p>
            <p>Your username is " . $_SERVER['REMOTE_USER'] . "</p>
            <p>Your IP adress is " . $_SERVER['REMOTE_ADDR'] . "</p>
        ");
      ?>
    </div>
  </content>

</body>
</html>
