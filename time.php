<?php include "header.php" ?>

  <div class="container">
    <?php
      $date = date('d.m.Y');
      $time = date('H:i:s');
      $day = date('w') - 1;
      $month = date('n') - 1;
      $week = date('W');

      print($date . "<br>");
      print($time . "<br>");
      print("<p>It is week number " . $week . "</p>");

      $months = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

      for($i = 0; $i <= count($months); $i++) {
        if($i == $month) {
          echo "<p>The month is: " . $months[$i] . "</p>";
        }
      }

      $days = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
      for($j = 0; $j <= count($days)+1; $j++) {
        if($j == $day) {
          echo "<p>The weekday is: " . $days[$j] . "</p>";
        }
      }
    ?>
  </div>

</body>
</html>
