<?php
  include "header.php";
  session_start();
 ?>

  <div class="container">
    <?php

      if(isset($_SESSION['username'])) {
        echo '<p>You are now logged in! Hello ' . $_SESSION['username'] . '!</p>';
        echo '<p><a href="destroysession.php"></p><button>Log out</button></a>';
      }
      else {
        echo '<p>You are not logged in!</p>';
        echo '<a href="setsession.php"><button>Log in</button></a>';
      }

    ?>
  </div>

</body>
</html>
